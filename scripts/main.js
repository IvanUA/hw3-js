//       1 
// Cycles are necessary and important part of Js because
//  they help us to manage a code by making it smaller, 
//  better readable and with less copies of the same strings.

//       2
// I will use cycle (For) in home work 2 instead of opearator (if) 
// which I used in my solution to avoid problem in case when
// user is adding input incorrecttly multyple times, cycle will help 
// to get correct input from user.

//       3
// We have two types (ways) of JavaScript conversions, the first one is processed
// by the use of a JavaScript functions (Methods: Number(), parseFloat(), parseInt(), etc)
//  and the 2nd one can be made by Js itself automatically (
//  5 + null  - returns 5         because null is converted to 0
// "5" + null - returns "5null"   because null is converted to "null"
// "5" + 2    - returns "52"      because 2 is converted to "2"
// "5" - 2    - returns 3         because "5" is converted to 5
// "5" * "2"  - returns 10        because "5" and "2" are converted to 5 and 2
// )


// Доброго вечора. 
// Повідомлення sorry, no numbers повинно бути виведено тільки 
// \1 раз
// Якщо numberFromUser менше 5. 
// Потрібно допрацювати і відправити знову.


let numberFromUser = prompt("Please enter your number", " number");

let numberUserInput = parseInt (numberFromUser);

for (let i = 1; i <= numberUserInput; ++i) {
    if (i % 5 === 0) {
        console.log(i);
    } else if (numberFromUser < 5) {
        console.log("Sorry, no numbers");
    }
}




// let numberFromUser = prompt("Please enter your number", " number");
// function myFunction (numberFromUser) {
//     if (numberFromUser % 5 === 0) {
//         console.log(numberFromUser);
//     } else if (!Number.isInteger(numberFromUser)) {
//         return myFunction;
// }
// }
// myFunction (numberFromUser);
